
// La création d'un Dnd requière un canvas et un interacteur.
// L'interacteur viendra dans un second temps donc ne vous en souciez pas au départ.


function DnD(canvas, interactor) {
	// Définir ici les attributs de la 'classe'

  this.xi = 0;
  this.xf = 0;
  this.yi = 0;
  this.yf = 0;
  this.canvas = canvas; 
  this.interactor = interactor;
  this.move = false;
  
  
  // Developper les 3 fonctions gérant les événements
this.onMousseDown = function(evt){
    var res = getMousePosition(canvas, evt);
    this.xi = res.x;
    this.yi = res.y;
    this.interactor.onInteractionStart(this);
    this.move = true;

}.bind(this)

this.onMousseUp = function(evt){
  var res = getMousePosition(canvas, evt);
  this.xf= res.x
  this.yf = res.y
  this.interactor.onInteractionEnd(this);
  this.move = false;

  //var rec = new Rectangle(this.xi,this.yi,this.xf, this.yf, 5, '#00CCC0');
  //rec.paint(canvas.getContext('2d'))
}.bind(this)


this.onMousseMove = function(evt){

  //rajouter un attribut pour activer cette fonction
  
  if(this.move){
    var res = getMousePosition(canvas, evt);
  this.xf= res.x
  this.yf = res.y
  this.interactor.onInteractionUpdate(this);

  }
  
}.bind(this)
  
 // Associer les fonctions précédentes aux évènements du canvas. 
 //ecrire une fonction pour recuperer  
 //canvas.addEventListener('click', this.onMousseDown , false);
 canvas.addEventListener('mousedown', this.onMousseDown , false);
 canvas.addEventListener('mousemove', this.onMousseMove, false);
 canvas.addEventListener('mouseup', this.onMousseUp, false);
};


// Place le point de l'événement evt relativement à la position du canvas.
function getMousePosition(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
};




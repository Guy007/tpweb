
var editingMode = { rect: 0, line: 1 };




function Pencil(ctx, drawing, canvas) {
	this.currEditingMode = editingMode.rect;
	this.currLineWidth = 5;
	this.currColour = '#000000';
	this.currentShape = 0;
	this.drawing =drawing;
	// Liez ici les widgets à la classe pour modifier les attributs présents ci-dessus.
	new DnD(canvas, this);

	var vals = document.getElementById('butRect');
	vals.addEventListener('click', ()=> {
		this.currEditingMode = editingMode.rect;
		console.log(this.currEditingMode)
	} , false);
	

	var vals1 = document.getElementById('butLine');
	vals1.addEventListener('click', ()=> {
		this.currEditingMode = editingMode.line;
		console.log(this.currEditingMode)
	} , false);
	var col = document.getElementById('colour');
	col.addEventListener('change', (evt)=> {
		this.currColour = evt.target.value;
		console.log(evt)
	} , false);
	var sp = document.getElementById('spinnerWidth');
	sp.addEventListener('change', (evt)=> {
		this.currLineWidth = evt.target.value;
		console.log(this.currLineWidth)
	} , false);


	this.onInteractionStart = function(dnd){
		if(editingMode.rect == this.currEditingMode){
			this.currentShape = new Rectangle(dnd.xi,dnd.yi,0,0,this.currLineWidth,this.currColour)
		}else if(editingMode.line == this.currEditingMode){
			this.currentShape = new Line(dnd.xi,dnd.yi,dnd.xf,dnd.yf,this.currLineWidth,this.currColour);
		}
	}
	this.onInteractionUpdate = function(dnd){
		this.drawing.paint(ctx)
		if(editingMode.rect == this.currEditingMode){
			this.currentShape = new Rectangle(dnd.xi,dnd.yi,dnd.xf - dnd.xi,dnd.yf - dnd.yi,this.currLineWidth,this.currColour)
		}else if(editingMode.line == this.currEditingMode){
			this.currentShape = new Line(dnd.xi,dnd.yi,dnd.xf,dnd.yf,this.currLineWidth,this.currColour);
			
		}
		console.log(this.currentShape);
		this.currentShape.paint(ctx)

	}
	this.onInteractionEnd = function(){
		this.drawing.forms.push(this.currentShape)
		this.drawing.paint(ctx)
	}

	// Implémentez ici les 3 fonctions onInteractionStart, onInteractionUpdate et onInteractionEnd
};



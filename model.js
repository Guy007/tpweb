
// Implémenter ici les 4 classes du modèle.
// N'oubliez pas l'héritage !

function Rectangle(xi,yi,xf,yf,epaisseur, couleur){
    Form.call(this,epaisseur, couleur);
    this.xi = xi;
    this.yi = yi;
    this.xf = xf;
    this.yf = yf;

    this.getInitX = function(){
        return this.xi;
    }

    this.getInitY = function(){
        return this.yi;
    }

   this.getFinalX = function(){
        return this.xf
    }

    this.getFinalY = function(){
        return this.yf
    }
    
}

function Line(xi,yi,xf,yf,epaisseur, couleur){
    Form.call(this,epaisseur, couleur);
    this.xi = xi;
    this.yi = yi;
    this.xf = xf;
    this.yf = yf;


    this.getInitX = function(){
        return this.xi;
    }

    this.getInitY = function(){
        return this.yi;
    }

   this.getFinalX = function(){
        return this.xf
    }

    this.getFinalY = function(){
        return this.yf
    }
}

function Form(epaisseur, couleur){
    this.epaisseur = epaisseur;
    this.couleur = couleur;
}

function Drawing(){
    this.forms = [];
    
    this.getForms = function(){
        return this.forms;
    }

}


